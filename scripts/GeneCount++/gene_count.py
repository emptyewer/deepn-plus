import os
import sys
from utils import test
from bintrees import AVLTree
import cutadapt


def get_current_dir():
    return os.getcwd()


def multiply(a, b):
    return a * b


def test_utils():
    return test()