QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += sdk_no_version_check
CONFIG += c++20

TARGET = DEEPN++
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    dqtextedit.cpp

HEADERS += \
    darkpaint.h \
    mainwindow.h \
    dqtextedit.h

FORMS += \
    mainwindow.ui

# Default rules for deployment
macx {
    ICON = ../icons/deepn.icns
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 13.0
}

win32 {
    ICON = ../icons/deepn.ico
}

unix {
}

RESOURCES += \
    deepn.qrc

